module.exports= function(grunt){
    // project configuration
    grunt.initConfig({
        concat: {
            js: {
                src: ['src/js/human.js', 'src/js/*.js'],
                dest: 'dist/js/script.js'
            }
        },
        copy: {
            js: {
                files: [
                    // includes files within path
                    {src: ['node_modules/jquery/dist/jquery.min.js'], dest: 'dist/js/jquery.min.js', filter: 'isFile'}
                ]
            }
        },
        uglify: {
            js: {
                files: {
                    'dist/js/script.min.js': ['dist/js/script.js']
                }
            }
        },
        resolve: {
            alias: {
                'vue$': 'vue/dist/vue.esm.js'
            }
        }

    });

    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify')

    grunt.registerTask('mep',['concat', 'uglify', 'copy', 'resolve']);
    grunt.registerTask('mep-js',['concat:js', 'uglify:js', 'copy:js', 'resolve']);
};