function getHTMLNews(news) {
	let div = createHtmlElement('div', '', ['col-md-4']);
	let title = createHtmlElement('h2', news['title'], []);
	let desc = createHtmlElement('p', news['description'], ['text-news']);
	div.append(title, desc); //permet de tout insérer en 1 fois

	return div;
}