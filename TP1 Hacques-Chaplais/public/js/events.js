function clickSearch(event){
	// bloque le submit d'un formulaire.
	event.preventDefault();
	reset();

	let inputSearch = $('#search'); // # permet de spécifier un ID
	if(inputSearch.val() === '') { // supprime les espaces
		$('#error-message').html('Recherche obligatoire !');
		return;
	}

	let elementsToSearch = $('.text-news'); // . permet de spécifier une classe
	let occurence = 0;

	elementsToSearch.each(element => occurence += highlightSearchFind(elementsToSearch.get(element), inputSearch.val()));
	updateH1(inputSearch.val(), occurence > 0);
}

function initEvents(){
	$('button').click(clickSearch);
	initEventNews();
}

function initEventNews() {
	let btnDetails = $('a');
	btnDetails.each(element => btnDetails.get(element).onclick = clickDetail(btnDetails.get(element)));
}