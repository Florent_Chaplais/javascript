iterate();

let allNewsDiv = document.querySelector('#allNews');

let h1 = document.querySelector('h1');
logMessageWithDate(h1.innerHTML);

JSON.parse(allNewsJSON).forEach(news => addNews(allNewsDiv, news));

let titleNews = document.querySelector('h3');
logMessageWithDate(titleNews.innerHTML);

let titles = document.querySelectorAll('.title');
titles.forEach(element => logMessageWithDate(element.innerHTML));

let button = document.querySelector('input[name="addNewsBtn"]');
bindButton(button);