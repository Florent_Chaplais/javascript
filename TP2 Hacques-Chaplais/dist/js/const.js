const ROUGE = '#FF0000'; //écrire en MAJ par convention
const BLEU = '#0000FF';
const VERT = '#008000';

const allNewsJSON =
    `[
	{"ID": 1, "title": "News 1", "description" : "Super news 1"},
	{"ID": 2, "title": "News 2", "description" : "Super news 2"},
	{"ID": 3, "title": "News 3", "description" : "Super news 3"}
]`;