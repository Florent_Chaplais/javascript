function bindButton(button){
	button.onclick = function(event){
		event.preventDefault();
    	let champ = document.querySelector('input[name="titleToAdd"]');
        let champ2 = document.querySelector('input[name="descToAdd"]');

		if(addArticle(champ.value, champ2.value))
			champ.value = '';
            champ2.value = '';

		return false;
	}
}

function clearErrors(){
	let errors = document.querySelectorAll('.error');

    if(errors){
        while(errors.length > 0 && errors[0].parentNode != null){
            errors[0].parentNode.removeChild(errors[0]);
        }        
    }
}

function addError(message, parent){
	let error = document.createElement('p');
    error.innerHTML = message;
    error.style.color = ROUGE;
    error.classList.add('error');

    parent.prepend(error);
}

function createHtmlElement(tag, label, classList, attributs){
    let element = document.createElement(tag);
    element.innerText = label;

    classList.forEach(function(item){
        element.classList.add(item);
    });

    if(attributs != undefined) {
        for (var key in attributs) {
            element.setAttribute(key, attributs[key]);
        }
    }

    return element;
}
