function createArticleHtml(news) {

    let div = createHtmlElement('div', '', ['col-md-4']);
    let id = createHtmlElement('id', news['ID'], []);
    let title = createHtmlElement('h3', news['title'], []);
    let desc = createHtmlElement('p', news['description'], ['text-news']);
    let a = createHtmlElement('a', 'View details »', ['btn', 'btn-secondary'], {'href': '#', 'role': 'button'});
    let pButton = document.createElement('p');

    id.hidden = true;
    desc.hidden = true;

    pButton.onclick = function (){
        if(desc.hidden){
            desc.hidden = false;
        }
        else desc.hidden = true;
    };
    pButton.append(a);
    div.append(title, id, desc, pButton);

    return div;
}

function checkArticleUnicity(title) {
    let h3s = document.querySelectorAll('.title');

    for (let i = 0; i < h3s.length; i++) {
        if (h3s[i].innerHTML.toLowerCase().trim() === title.toLowerCase().trim()) {
            let form = document.querySelector('#addNewsForm');
            addError('Erreur article deja existant', form);

            return false;
        }
    }

    return true;
}

function addArticle(title, desc) {
    clearErrors();

    if (!checkArticleUnicity(title) || !checkArticleUnicity(desc)) {
        return false;
    }

    addNews(allNewsDiv,{"title":  title, "description": desc});
    return true;
}


function addNews(parent, news) {
    parent.appendChild(createArticleHtml(news));
}
function createArticleHtml(news) {

    let div = createHtmlElement('div', '', ['col-md-4']);
    let id = createHtmlElement('id', news['ID'], []);
    let title = createHtmlElement('h3', news['title'], []);
    let desc = createHtmlElement('p', news['description'], ['text-news']);
    let a = createHtmlElement('a', 'View details »', ['btn', 'btn-secondary'], {'href': '#', 'role': 'button'});
    let pButton = document.createElement('p');

    id.hidden = true;
    desc.hidden = true;

    pButton.onclick = function (){
        if(desc.hidden){
            desc.hidden = false;
        }
        else desc.hidden = true;
    };
    pButton.append(a);
    div.append(title, id, desc, pButton);

    return div;
}

function checkArticleUnicity(title) {
    let h3s = document.querySelectorAll('.title');

    for (let i = 0; i < h3s.length; i++) {
        if (h3s[i].innerHTML.toLowerCase().trim() === title.toLowerCase().trim()) {
            let form = document.querySelector('#addNewsForm');
            addError('Erreur article deja existant', form);

            return false;
        }
    }

    return true;
}

function addArticle(title, desc) {
    clearErrors();

    if (!checkArticleUnicity(title) || !checkArticleUnicity(desc)) {
        return false;
    }

    addNews(allNewsDiv,{"title":  title, "description": desc});
    return true;
}


function addNews(parent, news) {
    parent.appendChild(createArticleHtml(news));
}
