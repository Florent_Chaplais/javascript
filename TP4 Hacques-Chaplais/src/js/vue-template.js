
const newsTemplate = `<article>
			              <h3>{{ article.title }}</h3>
			              <p>{{ article.description }}</p>
			              <button @click="">View detail</button>
		              </article>`;
const newsComponent = {
    props: ['article'],
    template: newsTemplate,
    methods: {
        log: logMessageWithDate
    }
};