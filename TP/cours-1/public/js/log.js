function displayMessageFromArray(array){
	array.forEach(element => logMessageConsole(element));
}

function logMessageConsole(message){
  console.log(getMessageFormatted(message));
}

function getMessageFormatted(message){
  let messageFormatted = "Message : " + message;
  
  return messageFormatted;
}